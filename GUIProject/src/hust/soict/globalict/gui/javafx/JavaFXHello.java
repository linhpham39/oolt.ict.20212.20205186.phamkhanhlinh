package hust.soict.globalict.gui.javafx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class JavaFXHello extends Application{
	private Button btnHello;		//declare a "Button" control
	
	public JavaFXHello() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public void start(Stage primaStage) {
		//construct the "Button" and attach an "EventHandler"
		btnHello = new Button();
		btnHello.setText("Say Hello");
		btnHello.setOnAction(evt->System.out.println("Hello World"));
		
		//construct a scene graph of nodes
		StackPane root = new StackPane();		//the root of scene graph is a layout node
		root.getChildren().add(btnHello);
		
		Scene scene = new Scene(root, 300, 100);		//construct a scene given the root of scene
		primaStage.setScene(scene);
		primaStage.setTitle("Hello");
		primaStage.show();
	}
	public static void main(String[] args) {
		launch(args);
	}
}
