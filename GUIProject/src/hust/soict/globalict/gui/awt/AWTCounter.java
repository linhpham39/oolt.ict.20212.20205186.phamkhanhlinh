package hust.soict.globalict.gui.awt;
//using awt container and component classes
import java.awt.*;
//using awt event classes and listener interfaces
import java.awt.event.*;

//inherit from the top-level container java.awt.frame
public class AWTCounter extends Frame implements ActionListener{
	private Label lblCount; 		//declare a label component
	private TextField tfCount; 		//declare a field component
	private Button btnCount; 		//declare a button component
	private int count = 0;			//count the number of time you click the buttom
	
	//setup gui components and even handlers
	public AWTCounter() {
		//"super" Frame � a container
		setLayout(new FlowLayout());
		//set the layout to FlowLayout
		//arrange component from left to right by default and flow 
		//from top to bottom
		lblCount = new Label("Count");		//construct the label component
		add(lblCount);						//"super" Frame container adds label component
		
		tfCount = new TextField(count + "", 10); //construct the textfield with initial value
		tfCount.setEditable(false);				//set to read-only
		add(tfCount);
		
		btnCount = new Button("Count");		//construct the button component
		add(btnCount);
		
		//actionListener is call when click button or menu item. has 1 method actionPerform
		btnCount.addActionListener(this);
		//"btnCount" is the source object that fires an actionEvent when clicked
		//the source add "this" instance as ActionEvent listener, 
		//click button invokes actionPerform()
		
		setTitle("AWT Counter example"); 		//set title
		setSize(250, 100);
		
		//inspecting the container/components objects
		System.out.println(this);
		System.out.println(lblCount);
		System.out.println(tfCount);
		System.out.println(btnCount);

		setVisible(true); 			// "super" Frame shows
		System.out.println(this);
		System.out.println(lblCount);
		System.out.println(tfCount);
		System.out.println(btnCount);
	}
	
	public static void main(String[] args) {
		AWTCounter app = new AWTCounter();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		++count;
		tfCount.setText(count + ""); //convert to string
	}
}
