package hust.soict.globalict.gui.swing;

import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class SwingDemo extends JFrame{

	//get the content-pane of this JFrame, which is a java.awt.container
	public SwingDemo() {
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		cp.add(new JLabel("Hello, world"));
		cp.add(new JButton("Button"));
	}

}
