package hust.soict.globalict.aims.utils;


public class DateUtils {
    public static void dateComparison(MyDate date1, MyDate date2) {
        if (date1.getYear() < date2.getYear()) {
            System.out.println(date1.printDate() + " begins earlier than " + date2.printDate());
        } else if (date1.getYear() > date2.getYear())
            System.out.println(date1.printDate() + " begins later than " + date2.printDate());
        else {
            if (date1.getMonth() < date2.getMonth()) {
                System.out.println(date1.printDate() + " begins earlier than " + date2.printDate());
            } else if (date1.getMonth() > date2.getMonth())
                System.out.println(date1.printDate() + " begins later than " + date2.printDate());
            else {
                if (date1.getDay() < date2.getDay()) {
                    System.out.println(date1.printDate() + " begins earlier than " + date2.printDate());
                } else if (date1.getDay() > date2.getDay())
                    System.out.println(date1.printDate() + " begins later than " + date2.printDate());
                else {
                    System.out.println("These two getDay()s are the same");
                }
            }
        }

    }

    public static void dateAscendingSorting(MyDate... date) {
        for (int i = 0; i < date.length; ++i) {
            for (int j = i; j < date.length; ++j) {
                if (isEarlierDate(date[j], date[i]) == true) {
                    MyDate temp = date[i];
                    date[i] = date[j];
                    date[j] = temp;
                }
            }
        }
    }

    public static boolean isEarlierDate(MyDate date1, MyDate date2) {
        if (date1.getYear() < date2.getYear()) {
            return true;
        } else if (date1.getYear() > date2.getYear())
            return false;
        else {
            if (date1.getMonth() < date2.getMonth()) {
                return true;
            } else if (date1.getMonth() > date2.getMonth())
                return false;
            else {
                if (date1.getDay() <= date2.getDay()) {
                    return true;
                } else
                    return false;
            }
        }

    }
}