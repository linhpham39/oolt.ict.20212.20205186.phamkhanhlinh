package hust.soict.globalict.aims.utils;

import java.time.LocalDateTime;
import java.util.Scanner;


public class MyDate {
    private int day;
    private int month;
    private int year;

    private final String[] arrMonth = { "", "January", "February", "March", "April", "May", "June", "July", "August",
    "September", "October", "November", "December" };

    private final String[] arrDay = { "", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth",
    "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth",
    "seventeenth", "eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second", "twenty-third",
    "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth", "twenty-ninth",
    "thirtieth", "thirty-first" };

    public MyDate(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }
     public MyDate(String day, String month, String year) {
        for (int i = 1; i < arrDay.length; i++) {
            if (arrDay[i].equals(day)) {
                this.day = i;
                break;
            }

        }
        for (int i = 1; i < arrMonth.length; i++) {
            if (arrMonth[i].equals(month)) {
                this.month = i;
                break;
            }

        }

        this.year = Integer.parseInt(year);
    }
    public MyDate() {
        this.day = LocalDateTime.now().getDayOfMonth();
        this.month = LocalDateTime.now().getMonthValue();
        this.year = LocalDateTime.now().getYear();
    }
    public MyDate(String date) {
        String dateParts[] = date.split("/");
        String dayPart = dateParts[0];
        String monthPart = dateParts[1];
        String yearPart = dateParts[2];
        this.day = Integer.parseInt(dayPart);
        this.month = Integer.parseInt(monthPart);
        this.year = Integer.parseInt(yearPart);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (day < 0 || day > 31) {
            System.out.println("Invalid day");
        } else {
            this.day = day;
        }
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month < 0 || month > 12) {
            System.out.println("Invalid month");
        } else {
            this.month = month;
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (year < 0) {
            System.out.println("Invalid year");
        } else {
            this.year = year;
        }
    }

    public static MyDate accept() {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a date (format: DD/MM/YYYY): ");
        String myDate = input.nextLine();
        input.close();
        return new MyDate(myDate);
    }

    public static void print(MyDate myDate) {
        System.out.println(myDate.getDay() + "/" + myDate.getMonth() + "/" + myDate.getYear());
    }
    
    public String printDate() {
		return day + "/" + month + "/" + year; 
    }
}


