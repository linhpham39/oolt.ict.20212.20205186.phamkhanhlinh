package hust.soict.globalict.aims.order.Order;


import java.util.ArrayList;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;


public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private MyDate dateOrdered;
	private static int nbOrders = 0;
	public static final int MAX_LIMITTED_ORDERED = 5;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private int id;
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	public static int getnbOrders() {
		return nbOrders;
	}

	public Order() {
		MyDate date = new MyDate();
		this.dateOrdered = date;
		if (nbOrders < MAX_NUMBERS_ORDERED)
			nbOrders++;
		else
			System.out.println("You can not add any order");

	}

	public void addMedia(Media item) {
		if(itemsOrdered.size() == MAX_LIMITTED_ORDERED) {
			System.out.println("The order is already full.");
		}
		else {
			itemsOrdered.add(item);
			item.setId(itemsOrdered.size());
			System.out.println("The media has been added");
		}
	}

	public void removeMedia(Media media) {
		if(itemsOrdered.size() == 0)
			System.out.println("The order is empty");
		else {
			int search = -1;
			for(int i = 0; i < itemsOrdered.size(); i++) {
				if(itemsOrdered.get(i).getTitle().equals(media.getTitle()));
				search = i;
			}
			if(search == -1)
				System.out.println("The media you want to delete is not in the list");
			else {
				itemsOrdered.remove(search);
				//update the id for remaining media
				for(int i = search; i < itemsOrdered.size(); i++)
					itemsOrdered.get(i).setId(i + 1);
				System.out.println("Remove media successfully");
			}
		}
	}
	public void removeMedia(int id) {
		if(itemsOrdered.size() == 0)
			System.out.println("The list is empty");
		else {
			int search = -1;
			for(int i = 0; i < itemsOrdered.size(); i++) {
				if(itemsOrdered.get(i).getId() == id)
				search = i;
			}
			if(search == -1)
				System.out.println("The media you want to delete is not in the list");
			else {
				//update the id for remaining media
				itemsOrdered.remove(search);
				for(int i = search; i < itemsOrdered.size(); i++)
					itemsOrdered.get(i).setId(i + 1);
				System.out.println("Remove media successfully");
			}
		}
	}

	public float totalCost() {
		float sum = 0f;
		for (int i = 0; i < itemsOrdered.size(); i++)
				sum += itemsOrdered.get(i).getCost();
		return sum;
	}

	public void printOrder() {
		System.out.println("*************************Order*************************************************************");
		System.out.println("Date[" + dateOrdered.printDate() + "]");
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.size(); ++i) {
			System.out.println((i + 1) + ". [" + itemsOrdered.get(i).getTitle() + "] - [" + itemsOrdered.get(i).getCategory()
				 + "]  - ["+ itemsOrdered.get(i).getCost() + "] $");
		}
		System.out.println("*******************************************************************************************");
	}

	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	
	//randomly pick out an item for free
	public int getALuckyItem(){
		int randomNumber = (int)(Math.random() * itemsOrdered.size());
		//itemsOrdered.get(randomNumber).setCost(0);
        return randomNumber;        
	}
}
