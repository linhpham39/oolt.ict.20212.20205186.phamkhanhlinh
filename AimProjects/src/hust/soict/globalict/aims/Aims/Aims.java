package hust.soict.globalict.aims.Aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order.Order;
import hust.soict.globalict.aims.utils.MemoryDaemon;

public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	public static int searchIdOrder(ArrayList<Order> userOrder,int id) {
		int search = -1;
		for(int i = 0; i < userOrder.size(); i++) 
			if(userOrder.get(i).getId() == id)
				 search = i;
		return search;
	}
	public static void main(String[] args) {
		showMenu();
		MemoryDaemon mDaemon = new MemoryDaemon();
		Thread thread = new Thread(mDaemon);
		thread.setDaemon(true);
		thread.start();
		Scanner sc = new Scanner(System.in);
		ArrayList<Order> userOrder = new ArrayList<Order>();
		int choice;
		do {
			choice = sc.nextInt();
			switch (choice) {
			case 1: 
				Order newOrder =new Order();
				userOrder.add(newOrder);
				newOrder.setId(userOrder.size());
				System.out.println("Create a new order with ID: "+ newOrder.getId());
				break;
			case 2:
				System.out.println("What order you want to add? Enter ID to search: ");
				int idOrder = sc.nextInt();
				//to read 'enter' when input integer
				String t = sc.nextLine();
				int search = searchIdOrder(userOrder, idOrder);
				if(search == -1)
					System.out.println("Order not found");
				else {
					System.out.println("Enter information for new item you want to add");
					System.out.println("DVD/Book/CD: ");
					String typeOfMedia = sc.nextLine();
					System.out.println("Title: ");
					String title = sc.nextLine();
					System.out.println("Category: ");
					String category = sc.next();
					System.out.println("Cost: ");
					float cost = sc.nextFloat();
					switch (typeOfMedia) {
					case "DVD": 
						DigitalVideoDisc DVD = new DigitalVideoDisc(title, category, cost);
						userOrder.get(search).addMedia(DVD);
						break;
					case "Book":
						Book book = new Book(title, category, cost);
						userOrder.get(search).addMedia(book);
						break;
					case "CD":
						CompactDisc CD = new CompactDisc(title, category, cost);
						System.out.println("Add track? (Y/N) ");
						t = sc.nextLine();
						if(sc.nextLine().charAt(0) == 'Y') {
							System.out.println("Number of tracks: ");
							int numTrack = sc.nextInt();
							for(int i = 0; i < numTrack; i++) {
								System.out.println("Title of track: "); String trTitle = sc.next();
								System.out.println("Length: "); int trLength = sc.nextInt();
								Track newTrack = new Track(trTitle, trLength);
								CD.addTrack(newTrack);
							}
						}
						userOrder.get(search).addMedia(CD);	
						CD.play();
					}
				}
				break;
			case 3:
				System.out.println("Enter the id of order: ");
				idOrder = sc.nextInt();
				search = searchIdOrder(userOrder, idOrder);
				if(search == -1)
					System.out.println("Not found order" + idOrder);
				else {				
					System.out.println("Enter the id of item");
					userOrder.get(search).removeMedia(sc.nextInt());
				}
				break;
			case 4:
				System.out.println("Enter order ID: ");
				idOrder = sc.nextInt();
				search = searchIdOrder(userOrder, idOrder);
				if(search == -1)
					System.out.println("Not found order" + idOrder);
				else {
					userOrder.get(search).printOrder();
				}
			}
		}while (choice != 0);
		sc.close();
	}
}