package hust.soict.globalict.aims.media;


public class DigitalVideoDisc extends Disc implements Playable {
	private String director;
	private int length;
	
	public String getDirector() {
		return director;
	}
	
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}


    public void setTitle(String title) {
       this.title = title;
    }
    
	public void setDirector(String director) {
		this.director = director;
	}
	public DigitalVideoDisc(String title) {
		this.title = title;
	}
	public DigitalVideoDisc(String title, String category) {
		this.title = title;
		this.category = category;
	}
	public DigitalVideoDisc(String title, String category, String director) {
		this.title = title;
		this.category = category;
		this.director = director;
	}
	
	public DigitalVideoDisc(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

	//find out (case insensitive) the corresponding disk of the current object containing the title
	public boolean search(String title){
		String temp = this.getTitle().toLowerCase();
		return temp.contains(title.toLowerCase());
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD: "+ this.getTitle());
		System.out.println("DVD length: "+ this.getLength());
	}
}
