package hust.soict.globalict.aims.media;

public class Track implements Playable, Comparable{
	private String title;
	private int length;
	

	public String getTitle() {
		return title;
	}


	public int getLength() {
		return length;
	}


	public Track() {
		// TODO Auto-generated constructor stub
	}


	public Track(String title, int length) {
		this.title = title;
		this.length = length;
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Media) {
			return this.title.equals(((Track)o).title) && this.length == ((Track)o).length;
		}
		return false;
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing Track: "+ this.getTitle());
		System.out.println("Playing length:" + this.getLength());
	}
	
	public int compareTo(Object obj) {
		if(!(obj instanceof Track))
			return -1;
		Track temp = (Track)obj;
		int cmp = this.title.compareTo(temp.getTitle());
		if(cmp != 0)
			return cmp;
		if(this.length != temp.getLength())
			return this.length > temp.getLength() ? 1 : -1;
		return 1;
	}

}
