package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{
	private List<String> authors = new ArrayList<String>();
	
	public Book(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	public void addAuthor(String authorName) {
		for(String i: authors)
			if(i.equals(authorName)) {
				System.out.println("Author is already in arraylist");
				return;
			}
		authors.add(authorName);
		System.out.println("Adding successfully");
	}

	public void removeAuthor(String authorName) {
		for(int i = 0; i < authors.size(); i++)
			if(authors.get(i).equals(authorName)) {
				authors.remove(i);
				System.out.println("Remove successfully");
				return;
			}
		System.out.println("The author is not in the list");
	}	
}
