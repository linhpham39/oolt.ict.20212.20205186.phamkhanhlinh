package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable,Comparable{
	private String artist;
	ArrayList<Track> tracks = new ArrayList<Track>();
	
	public int getLength() {
		int length = 0;
		for(int i = 0; i < this.tracks.size(); i++) {
			length += this.tracks.get(i).getLength();
		}
		return length;
	}
	
	public String getArtist() {
		return artist;
	}


	public CompactDisc() {
		// TODO Auto-generated constructor stub
		
	}


	public CompactDisc(String title, String category, float cost) {
		super(title, category, cost);
	}
	public void addTrack(Track track) {
		if(tracks.contains(track))
			System.out.println("The track has been already in the list");
		else {
			tracks.add(track);
			System.out.println("Add track successfully");
		}
	}
	
	public void removeTrack(Track track) {
		if(!this.tracks.contains(track))
			System.out.println("The track is not in the list");
		else {
			this.tracks.remove(track);
			System.out.println("Delete track successfully");
		}
	}

	@Override
	public void play() {
		for(Track t: tracks) {
			//t.play();
			System.out.println("# "+ tracks.indexOf(t) + " Title: "+ t.getTitle() + " Length: "+ t.getLength());
		}
	}

	@Override
	public int compareTo(Object o) {
		if(!(o instanceof CompactDisc))
			return -1;
		CompactDisc cd = (CompactDisc)o;
		if(this.tracks.size() != cd.tracks.size())
			return this.tracks.size() > cd.tracks.size() ? 1 : -1;
		else {
			int cmp = this.title.compareTo(cd.title);
			return cmp;
		}
	}
}
