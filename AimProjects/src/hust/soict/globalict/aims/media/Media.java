package hust.soict.globalict.aims.media;

public abstract class Media {
	 String title;
	 String category;
	 float cost;
	 int id;
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}


	public String getCategory() {
		return category;
	}




	public float getCost() {
		return cost;
	}
	@Override
	public boolean equals(Object o) {
		//check if o is kind of media
		if(o instanceof Media) {
			return this.id == ((Media)o).id;
		}
		return false;
	}

}
