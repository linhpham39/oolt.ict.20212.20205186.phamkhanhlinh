package hust.soict.globalict.test.utils.DateTest;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
   public static void main(String[] args) {
       MyDate date1 = new MyDate(1, 11, 2002);
       MyDate date2 = new MyDate("first", "January", "2022");
       MyDate date3 = new MyDate("3/9/2002");
       MyDate[] dMyDates = {date1, date2, date3};
       DateUtils.dateComparison(date1, date2);
       DateUtils.dateComparison(date1, date3);
       DateUtils.dateAscendingSorting(dMyDates);
       for (int i = 0; i < dMyDates.length; i++) {
        System.out.println(dMyDates[i].printDate());
         System.out.println();
      }
        date1.printDate();
   } 
}
