package hust.soict.globalict.garbage;
import java.util.Random;

public class GarbageCreator {
    public static void main(String[] args) {
        String s = "";
        long start = System.currentTimeMillis();
        Random r = new Random(123);
        for(int i = 0; i < 100000; i++)
           s += r.nextInt(2);
        System.out.println(System.currentTimeMillis()- start);
    }
}
