
package hust.soict.globalict.garbage;
import java.util.Random;

public class NoGarbage {
    public static void main(String[] args) {
        StringBuffer s = new StringBuffer();
        Random r = new Random();
        long start = System.currentTimeMillis();
        for(int i = 0; i < 100000; i++)
            s.append(r.nextInt(2));
        System.out.println(System.currentTimeMillis() - start);
    }
}
